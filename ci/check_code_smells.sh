#!/usr/bin/env bash
set -euo pipefail

source ./venv/bin/activate
pylint "./app" --rcfile=pylintrc
pylint tests --rcfile=pylintrc-test
