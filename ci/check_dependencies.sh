#!/usr/bin/env bash
set -euo pipefail

source ./venv/bin/activate
safety check --full-report -i 70612
