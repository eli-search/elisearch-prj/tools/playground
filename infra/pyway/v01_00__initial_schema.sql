CREATE TABLE IF NOT EXISTS job_config (
  job_id varchar(250) NOT NULL,
  spider_name varchar(250) NOT NULL,
  sitemap_url varchar(250) NOT NULL,
  atom_url varchar(250) NOT NULL,
  domain_url varchar(250),
  execution_time varchar(250),
  sitemap_interval varchar(250),
  atom_interval varchar(250),
  output_path varchar(250) NOT NULL,
  output_bucket varchar(250) NOT NULL,
  output_working_bucket varchar(250) NOT NULL,
  resource_delay int,
  max_resource_retries int,
  eli_source_delay int,
  max_eli_source_retries int,
  PRIMARY KEY (job_id)
);

CREATE TABLE IF NOT EXISTS resource_data (
  eli_uri varchar(250) NOT NULL,
  job_id varchar(250) NOT NULL,
  metadata_path varchar(250),
  metadata_timestamp timestamptz,
  raw_data_path varchar(250),
  raw_data_timestamp timestamptz,
  last_update timestamptz NOT NULL,
  last_indexation timestamptz,
  deleted boolean NOT NULL DEFAULT FALSE,
  PRIMARY KEY (eli_uri),
  FOREIGN KEY (job_id) REFERENCES job_config(job_id)
);

CREATE TYPE job_status AS ENUM ('SUCCESS', 'FAIL');

CREATE TABLE IF NOT EXISTS execution_status (
  execution_id varchar(250) NOT NULL,
  job_id varchar(250) NOT NULL,
  start_date timestamptz,
  end_date timestamptz,
  status job_status,
  error_msg varchar(250),
  PRIMARY KEY (execution_id),
  FOREIGN KEY (job_id) REFERENCES job_config(job_id)
);

CREATE TYPE type AS ENUM ('RDF', 'FTS');

CREATE TABLE IF NOT EXISTS filtering_queries (
  id varchar(250) NOT NULL,
  name varchar(250) NOT NULL,
  query_type type NOT NULL,
  content varchar NOT NULL,
  enabled boolean NOT NULL DEFAULT TRUE,
  PRIMARY KEY (id)
);