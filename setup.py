from setuptools import setup, find_packages


setup(
    name='local',
    version='local',
    packages=find_packages(exclude=["tests*"]),
    data_files=["requirements.txt", "setup.py"],
    entry_points={'scrapy': ['settings = app.harvester.settings']}
)
