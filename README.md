# ELI SEARCH - Ingestion Application


## Prerequisites
- Python 3.11
- docker
- docker-compose

### Libraries
## Create a virtual environment

We advise to create a virtual environment dedicated to this project in which all the required libraries will be installed.

```shell
python -m venv venv
source ./venv/bin/activate
```

## Install the requirements
You can now install all the necessary libraries by executing: 

```shell
pip install -r requirements.txt
```


### Environment Variables

Before executing any script, you have to set up the environment variables:

```shell
export $(cat .env | cut -d# -f1 | xargs)
```

You can update the environment variables in the `.env` file.

### Run locally 

First, start the local DB: 

```shell
cd infra
docker-compose up -d
cd ..
```

You can check if you DB schema is up-to-date using Pyway:
```shell
pyway info
```

If a new version of the DB is available, validate the configuration and execute the Pyway migration scripts:
```shell
pyway validate
pyway migrate
```

Then, you can run the ingestion app:
```shell
python app/app.py --job_id <job_id> --scraping_source=<scraping_source>
```

Build and run docker image locally: 
```shell
docker build -t ingestion-app .
docker run --env-file .env --network="host" ingestion-app --job_id=<job_id> --scraping_source=<scraping_source>
```

#### Available arguments: 

--job_id: Selects the ingestion's job to run [**required**]\
--scraping_source: Selects the datasource to crawl. Accepted values: `ATOM_FEED` or `SITEMAP` [**required**]\
--enable_indexing: Enables the indexing part of the ingestion (only available on AWS). Disabled by default. \
--debug: Sets the log level to `DEBUG`. Log level sets to `INFO` by default. 