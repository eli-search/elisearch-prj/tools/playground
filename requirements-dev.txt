bandit==1.7.8
black==24.4.2
mypy==1.10.0
pre-commit==3.7.1
pycodestyle==2.11.1
pylint==3.2.2
safety==3.2.1