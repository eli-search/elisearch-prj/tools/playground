import json
import logging
import os

import boto3

from opensearchpy import OpenSearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth

from aws.s3_client import S3Client
from aws.ssm_client import SSMClient

logger = logging.getLogger(__name__)


environment = os.environ["ENV"]
SSM_OPENSEARCH_HOST = f"/elisearch/{environment}/opensearch/host"


class OpensearchClient:
    def __init__(self, region_name="eu-west-1"):
        session = boto3.session.Session(region_name=region_name)
        credentials = session.get_credentials()
        opensearch_host = SSMClient().get_ssm_parameter(SSM_OPENSEARCH_HOST)
        awsauth = AWS4Auth(
            credentials.access_key,
            credentials.secret_key,
            region_name,
            "es",
            session_token=credentials.token,
        )
        self.search = OpenSearch(
            hosts=[{"host": opensearch_host, "port": 443}],
            http_auth=awsauth,
            use_ssl=True,
            verify_certs=True,
            http_compress=True,
            connection_class=RequestsHttpConnection,
        )
        logger.info(f"OpenSearch client created with host: {opensearch_host}")

    search: OpenSearch

    def bulk_index(
        self, index_name: str, bucket: str, prefix: str, extension: str
    ) -> None:
        if not self.search.indices.exists(index_name):
            logger.error(
                f"Impossible to index data in Opensearch, index {index_name} doesn't exist"
            )
            return

        s3_client = S3Client()
        iterator = s3_client.get_file_iterator(bucket, prefix)
        for response in iterator:
            bulk_data: list[dict[str, str] | dict[str, dict[str, str]]] = []
            for object_data in response["Contents"]:
                key = object_data["Key"]
                if key.endswith(f".{extension}"):
                    json_object = s3_client.get_object_as_dict(bucket, key)
                    if json_object:
                        object_id = json_object["id"]
                        bulk_data.append(
                            {"index": {"_index": index_name, "_id": object_id}}
                        )
                        bulk_data.append({"value": json.dumps(json_object)})
            rc = self.search.bulk(bulk_data)  # TODO: if nothing inside bulk_data ?
            if rc["errors"]:
                logger.error(f"Error during bulk index in {index_name}")
                for item in rc["items"]:
                    # TODO: Raise exception?
                    logger.error(
                        f"{item['index']['status']}: {item['index']['error']['type']}"
                    )
            else:
                logger.info(f"Bulk inserted {len(rc['items'])} items in {index_name}")
