import logging
import os
import time

import boto3

from aws.ssm_client import SSMClient

logger = logging.getLogger(__name__)


environment = os.environ["ENV"]
SSM_NEPTUNE_WRITER_ENDPOINT = f"/elisearch/{environment}/neptune/writer_endpoint_url"
SSM_NEPTUNE_BULK_ROLE = f"/elisearch/{environment}/iam/neptune_bulk_loader_role_arn"


class NeptuneClient:
    def __init__(self, region_name="eu-west-1"):
        session = boto3.session.Session(region_name=region_name)
        neptune_writer_endpoint = SSMClient().get_ssm_parameter(
            SSM_NEPTUNE_WRITER_ENDPOINT
        )
        self.client = session.client(
            service_name="neptunedata",
            endpoint_url=f"https://{neptune_writer_endpoint}:8182",
        )

    def get_status(self, load_id: str) -> tuple[int, int, int]:
        response = self.client.get_loader_job_status(loadId=load_id, details=True)
        payload = response.get("payload", {})

        load_completed = 0
        load_in_progress = 0
        load_not_started = 0

        if "feedCount" in payload.keys():
            for element in payload["feedCount"]:
                if "LOAD_NOT_STARTED" in element.keys():
                    load_not_started = element["LOAD_NOT_STARTED"]
                if "LOAD_IN_PROGRESS" in element.keys():
                    load_in_progress = element["LOAD_IN_PROGRESS"]
                if "LOAD_COMPLETED" in element.keys():
                    load_completed = element["LOAD_COMPLETED"]
        return load_completed, load_in_progress, load_not_started

    def check_progress(self, load_id: str) -> None:
        running = True
        while running:
            load_completed, load_in_progress, load_not_started = self.get_status(
                load_id
            )
            logger.info(
                f"Load completed: {load_completed}, in progress: {load_in_progress}, not started: {load_not_started}"
            )
            if not load_in_progress and not load_not_started:
                running = False
            time.sleep(5)

    def start_loader_job(self, bucket: str, prefix: str) -> None:
        logger.info(f"Starting Neptune bulk loader job on {bucket}/{prefix}")
        response = self.client.start_loader_job(
            source=f"s3://{bucket}/{prefix}",
            format="turtle",
            mode="AUTO",
            s3BucketRegion="eu-west-1",
            iamRoleArn=SSMClient().get_ssm_parameter(SSM_NEPTUNE_BULK_ROLE),
            failOnError=True,
            parallelism="LOW",
        )
        load_id = response["payload"]["loadId"]
        self.check_progress(load_id)
