class InvalidScrapingSourceException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class SpiderException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class GetConfigException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class S3UploadException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class GetSecretException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class BadOutputPathException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class DatabaseException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class GetSSMException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message
