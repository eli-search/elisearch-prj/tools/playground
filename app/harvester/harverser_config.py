import logging

from app_config import AppConfig
from harvester.common.exceptions import (
    InvalidScrapingSourceException,
    BadOutputPathException,
)
from harvester.common.models import ScrapingSource
from harvester.parser.atom_feed_parser import AtomFeedParser
from harvester.parser.sitemap_parser import SitemapParser
from harvester.parser.url_parser import UrlParser
from harvester.writer.local_writer import LocalWriter
from harvester.writer.s3_writer import S3Writer
from harvester.writer.file_writer import FileWriter

logger = logging.getLogger(__name__)


class HarvesterConfig(AppConfig):
    def __init__(self, input_data: dict):
        super().__init__(input_data)
        logger.info("Start loading the harvester configuration")
        self.scraping_source = self.get_scraping_source()
        self.url_parser = self.get_url_parser()
        self.writer = self.get_file_writer()
        logger.info("Harvester configuration loaded successfully")

    scraping_source: ScrapingSource
    url_parser: UrlParser
    writer: FileWriter

    def get_scraping_source(self) -> ScrapingSource:
        scraping_source_input = self.input_data["scraping_source"]
        try:
            source = ScrapingSource[scraping_source_input]
            if source == ScrapingSource.ATOM_FEED and not self.job_config.atom_url:
                raise InvalidScrapingSourceException(
                    f"Atom Feed URL not available for job_id: {self.job_config.job_id}"
                )
            return source
        except KeyError as err:
            raise InvalidScrapingSourceException(
                f'Invalid source name "{scraping_source_input}", valid values : {[e.value for e in ScrapingSource]}'
            ) from err

    def get_url_parser(self):
        logger.info(
            f"Retrieving XML parser based on scraping source: {self.scraping_source}"
        )
        if self.scraping_source is ScrapingSource.ATOM_FEED:
            return AtomFeedParser(
                self.job_config.atom_url, self.input_data["last_scraping_date"]
            )
        return SitemapParser(self.job_config.sitemap_url)

    def get_file_writer(self) -> FileWriter:
        logger.info(
            f"Retrieving writer based on setting local_process={self.local_process}"
        )
        if self.local_process:
            output_path = self.job_config.output_path
            if not output_path.startswith("s3://"):
                return LocalWriter()
            raise BadOutputPathException(
                f"The output_path for a local process must not be an S3 path. "
                f"Provided value: {output_path}"
            )
        output_bucket = self.job_config.output_bucket
        output_working_bucket = self.job_config.output_working_bucket
        if not output_bucket.startswith(
            "s3://"
        ) and not output_working_bucket.startswith("s3://"):
            return S3Writer()
        raise BadOutputPathException(
            f"Expecting a bucket name for output_bucket and output_working_bucket."
            f"Provided values: {output_bucket} and {output_working_bucket}"
        )
