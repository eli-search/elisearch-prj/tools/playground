from typing import Any
import logging
import scrapy
from scrapy.spiders import Spider
from harvester.common.models import Item


logger = logging.getLogger()


class Spain(Spider):
    name = "spain"
    allowed_domains = ["boe.es"]
    scraped_ids: set[str] = set()
    urls: list[dict[str, str]]

    def start_requests(self):
        for element in self.urls:
            yield scrapy.Request(
                url=element["url"],
                cb_kwargs={"last_mod_date": element["last_mod_date"]},
            )

    def parse(self, response, **kwargs) -> Any:
        item = Item(
            uri=response.url,
            last_mod_date=kwargs.get("last_mod_date"),
            raw_data=response.body,
            is_realized_by=response.xpath(
                ".//meta[contains(@property,'#is_realized_by')]/@resource"
            ).getall(),
            title=response.xpath(
                ".//meta[contains(@property,'#title')]/@content"
            ).extract_first(),
            type_document=response.xpath(
                ".//meta[contains(@property,'#type_document')]/@content"
            ).extract_first(),
            is_embodied_by=response.xpath(
                ".//meta[contains(@property,'#is_embodied_by')]/@resource"
            ).getall(),
            id_local=response.xpath(
                ".//meta[contains(@property,'#id_local')]/@content"
            ).extract_first(),
            date_document=response.xpath(
                ".//meta[contains(@property,'#date_document')]/@content"
            ).extract_first(),
            language=response.xpath(
                ".//meta[contains(@property,'#language')]/@resource"
            ).extract_first(),
            date_publication=response.xpath(
                ".//meta[contains(@property,'#date_publication')]/@content"
            ).extract_first(),
            embodies=response.xpath(
                ".//meta[contains(@property,'#embodies')]/@resource"
            ).getall(),
            realizes=response.xpath(
                ".//meta[contains(@property,'#realizes')]/@resource"
            ).getall(),
        )
        self.scraped_ids.add(item.uri)
        yield item
