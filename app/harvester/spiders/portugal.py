from typing import Any
import scrapy
from scrapy.spiders import Spider
from scrapy_playwright.page import PageMethod
from harvester.common.models import Item


class Portugal(Spider):
    name = "portugal"
    allowed_domains = ["data.dre.pt"]
    scraped_ids: set[str] = set()
    urls: list[dict[str, str]]

    def start_requests(self):
        for element in self.urls:
            yield scrapy.Request(
                url=element["url"],
                meta={
                    "playwright": True,
                    "playwright_page_methods": [
                        PageMethod(
                            "wait_for_selector",
                            selector="#eli-legal-resource",
                            state="attached",
                            timeout=60000,
                        )
                    ],
                },
                cb_kwargs={"last_mod_date": element["last_mod_date"]},
            )

    def parse(self, response, **kwargs) -> Any:
        item = Item(
            uri=response.xpath(
                ".//span[contains(@typeof,'eli:LegalResource')]/@about"
            ).extract_first(),
            last_mod_date=kwargs.get("last_mod_date"),
            raw_data=response.body,
            date_publication=response.xpath(
                ".//span[contains(@property,'eli:date_publication')]/@resource"
            ).extract_first(),
            embodies=response.xpath(
                ".//span[contains(@property,'eli:embodies')]/@resource"
            ).getall(),
            id_local=response.xpath(
                ".//span[contains(@property,'eli:id_local')]/@resource"
            ).extract_first(),
            in_force=response.xpath(
                ".//span[contains(@property,'eli:in_force')]/@resource"
            ).getall(),
            is_realized_by=response.xpath(
                ".//span[contains(@property,'eli:is_realized_by')]/@resource"
            ).getall(),
            language=response.xpath(
                ".//span[contains(@property,'eli:language')]/@resource"
            ).extract_first(),
            realizes=response.xpath(
                ".//span[contains(@property,'eli:realizes')]/@resource"
            ).getall(),
            title=response.xpath(
                ".//span[contains(@property,'eli:title')]/@resource"
            ).extract_first(),
            transposes=response.xpath(
                ".//span[contains(@property,'eli:transposes')]/@resource"
            ).getall(),
            type_document=response.xpath(
                ".//span[contains(@property,'eli:type_document')]/@resource"
            ).extract_first(),
        )
        self.scraped_ids.add(item.uri)
        yield item
