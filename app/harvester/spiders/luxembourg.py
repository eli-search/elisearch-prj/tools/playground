from typing import Any
import scrapy
from scrapy.spiders import Spider
from scrapy_playwright.page import PageMethod
from harvester.common.models import Item


class Luxembourg(Spider):
    name = "luxembourg"
    allowed_domains = ["legilux.public.lu"]
    scraped_ids: set[str] = set()
    urls: list[dict[str, str]]

    def start_requests(self):
        for element in self.urls:
            url = element["url"]
            if "consolide" not in url and "eli/etat/leg" in url:
                yield scrapy.Request(
                    url=url,
                    meta={
                        "playwright": True,
                        "playwright_page_methods": [
                            PageMethod(
                                "wait_for_selector",
                                selector="meta[typeof*='ontology#']",
                                state="detached",
                            )
                        ],
                    },
                    cb_kwargs={"last_mod_date": element["last_mod_date"]},
                )

    def parse(self, response, **kwargs) -> Any:
        # TODO: Fix when website contains ELI metadata again
        # eli_properties = response.xpath("//meta[contains(@property,'eli')]/@property").getall()
        # transposes = response.xpath(".//meta[contains(@property,'transposes')]/@property").getall()

        item = Item(
            uri="",
            last_mod_date=kwargs.get("last_mod_date"),
            raw_data=response.body,
            date_document="",
            date_publication="",
            is_about="",
            is_embodied_by="",
            is_realized_by="",
            language="",
            title="",
            transposes="",
            type_document="",
        )
        self.scraped_ids.add(item.uri)
        yield item
