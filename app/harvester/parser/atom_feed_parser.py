import logging
from dataclasses import dataclass
from datetime import datetime
from typing import List
from harvester.parser.url_parser import UrlParser

logger = logging.getLogger(__name__)


@dataclass
class FeedEntries:
    title: str
    id: str
    updated: datetime


def get_feed_entries(xml) -> List[FeedEntries]:
    entries = xml.find_all("entry")
    output = []
    for entry in entries:
        feed_entry = FeedEntries(
            title=entry.find("title").text,
            id=entry.find("id").text,
            updated=datetime.fromisoformat(entry.find("updated").text),
        )
        output.append(feed_entry)
    return output


class AtomFeedParser(UrlParser):
    def __init__(self, url: str, last_scraping_date: datetime):
        super().__init__(url)
        self.last_scraping_date = last_scraping_date

    last_scraping_date: datetime

    def get_urls(self) -> list[dict[str, str]]:
        logger.info(f"Retrieving URLs from Atom Feed: {self.url}")
        output = []
        xml = super().get_xml_content()
        entries = get_feed_entries(xml)
        for entry in entries:
            timezone = entry.updated.tzinfo
            if entry.updated > self.last_scraping_date.astimezone(timezone):
                output.append(
                    {
                        "url": entry.id,
                        "last_mod_date": entry.updated.strftime("%Y-%m-%dT%H-%M-%S"),
                    }
                )
        logger.info(f"{len(output)} URLs retrieved")
        return output
