from abc import abstractmethod

import requests
from bs4 import BeautifulSoup


class UrlParser:
    def __init__(self, url):
        self.url = url

    url: str

    @abstractmethod
    def get_urls(self) -> list[dict[str, str]]:
        raise NotImplementedError("Cannot call an abstract method, must be override")

    def get_xml_content(self) -> BeautifulSoup:
        response = requests.get(self.url, timeout=20)
        xml = BeautifulSoup(response.content, "lxml-xml")
        return xml
