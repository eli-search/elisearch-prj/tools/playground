# General
SPIDER_MODULES = ["harvester.spiders"]
NEWSPIDER_MODULE = "harvester.spiders"
ROBOTSTXT_OBEY = True
REQUEST_FINGERPRINTER_IMPLEMENTATION = "2.7"

# Item pipelines
ITEM_PIPELINES = {
    "harvester.pipelines.store_sources.StoreSources": 1,
    "harvester.pipelines.generate_and_store_triples.GenerateAndStoreTriples": 2,
    "harvester.pipelines.generate_and_store_jsonlds.GenerateAndStoreJsonLDs": 3,
    "harvester.pipelines.mark_deleted_resources.MarkDeletedResources": 1000,
}

# Playwright
DOWNLOAD_HANDLERS = {
    "http": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler",
    "https": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler",
}
TWISTED_REACTOR = "twisted.internet.asyncioreactor.AsyncioSelectorReactor"
PLAYWRIGHT_LAUNCH_OPTIONS = {"headless": True}

# TODO: to remove for production
CLOSESPIDER_PAGECOUNT = 1000
