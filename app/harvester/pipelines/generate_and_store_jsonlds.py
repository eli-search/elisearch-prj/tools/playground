import json
import logging
from harvester.common.models import Item, QueryType
from harvester.pipelines.common import (
    generate_graph,
    store_result_files,
    apply_enrichment_and_filtering_queries,
)
from rdflib import Graph
from rdflib.plugins.sparql import prepareQuery
from itemadapter import ItemAdapter

logger = logging.getLogger(__name__)
FTS_INDEX_CONTEXT_FILENAME = "app/config/fts_index_context.json"
FTS_ELI_URI_QUERY_FILENAME = "app/config/fts_index_eli_uri_sparql.properties"
FTS_EXTRACT_METADATA_QUERY_FILENAME = (
    "app/config/fts_extract_metadata_sparql.properties"
)


def load_fts_index_context() -> dict:
    logger.debug(
        f"Loading the FTS index context from file {FTS_INDEX_CONTEXT_FILENAME}"
    )
    with open(FTS_INDEX_CONTEXT_FILENAME, encoding="UTF-8") as file:
        context = json.load(file)
        logger.debug("FTS index context loaded successfully")
        return context


def get_fts_index_eli_uri_sparql_query(filename: str) -> str:
    logger.debug(f"Retrieving the FTS index Sparql query from file {filename}")
    with open(filename, encoding="UTF-8") as file:
        content = file.read()
        logger.debug("FTS index Sparql query retrieved successfully")
        return content


class GenerateAndStoreJsonLDs:
    def process_item(self, item: Item, spider):
        item_adapter = ItemAdapter(item)
        uri = item_adapter["uri"]
        logger.debug(f"Generating JSON-LD file for resource: {uri}")
        harvester_config = spider.settings["HARVESTER_CONFIG"]
        g = generate_graph(item_adapter)

        logger.debug("Extracting identifiers from raw graph")
        eli_uri_query_str = get_fts_index_eli_uri_sparql_query(
            FTS_ELI_URI_QUERY_FILENAME
        )
        eli_uri_query = prepareQuery(eli_uri_query_str.replace("$URL", uri))
        for identifier in g.query(eli_uri_query):
            logger.debug(
                f"Extracting metadata from raw graph for identifier: {identifier}"
            )
            resource_graph = Graph()
            metadata_query_str = get_fts_index_eli_uri_sparql_query(
                FTS_EXTRACT_METADATA_QUERY_FILENAME
            )
            metadata_query = prepareQuery(
                metadata_query_str.replace(
                    "http://eli_uri", identifier.asdict()["eli_uri"]
                )
            )
            resource_graph += g.query(metadata_query)  # TODO: change ?

            enriched_graph = apply_enrichment_and_filtering_queries(
                uri, harvester_config, resource_graph, QueryType.FTS
            )

            logger.debug("Applying context to the graph")
            content = enriched_graph.serialize(
                format="json-ld", context=load_fts_index_context()
            )
            logger.debug("Context successfully applied")

            logger.debug(f"Storing JSON-LD file for resource: {uri}")
            store_result_files(
                harvester_config,
                uri,
                item_adapter["last_mod_date"],
                content,
                "jsonld",
                False,
                True,
            )
            logger.debug(
                f"JSON-LD file for resource: {uri} successfully generated and stored"
            )
        return item
