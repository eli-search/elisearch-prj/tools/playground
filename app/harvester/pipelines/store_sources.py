import logging
from datetime import datetime
from itemadapter import ItemAdapter
from db.db_client import DBClient
from db.db_models import ResourceData
from harvester.common.models import Item
from harvester.pipelines.common import store_result_files

logger = logging.getLogger(__name__)


class StoreSources:
    def process_item(self, item: Item, spider):
        item_adapter = ItemAdapter(item)
        uri = item_adapter["uri"]
        logger.debug(f"Storing sources for resource: {uri}")
        harvester_config = spider.settings["HARVESTER_CONFIG"]
        path = store_result_files(
            harvester_config,
            uri,
            item_adapter["last_mod_date"],
            item_adapter["raw_data"],
            "html",
            True,
            False,
        )
        logger.debug(f"Updating resource_data table for resource: {uri}")
        resource_data = ResourceData(
            eli_uri=item.uri,
            job_id=harvester_config.job_config.job_id,
            raw_data_path=path,
            raw_data_timestamp=datetime.now(),
        )
        DBClient(harvester_config.db_config).update_or_create(resource_data, "eli_uri")
        logger.debug(f"resource_data table successfully updated for resource: {uri}")
        logger.debug(f"Sources for resource: {uri} successfully stored")
        return item
