import logging
from datetime import datetime

from itemadapter import ItemAdapter

from db.db_client import DBClient
from db.db_models import ResourceData
from harvester.common.models import Item, QueryType
from harvester.pipelines.common import (
    apply_enrichment_and_filtering_queries,
    generate_graph,
    store_result_files,
)

logger = logging.getLogger(__name__)


class GenerateAndStoreTriples:
    def process_item(self, item: Item, spider):
        item_adapter = ItemAdapter(item)
        uri = item_adapter["uri"]
        last_mod_date = item_adapter["last_mod_date"]
        logger.debug(f"Generating triples file for resource: {uri}")
        harvester_config = spider.settings["HARVESTER_CONFIG"]

        g = generate_graph(item_adapter)
        store_result_files(
            harvester_config, uri, last_mod_date, g.serialize(), "ttl", True, False
        )

        g = apply_enrichment_and_filtering_queries(
            uri, harvester_config, g, QueryType.RDF
        )
        path = store_result_files(
            harvester_config, uri, last_mod_date, g.serialize(), "ttl", False, True
        )

        logger.debug(f"Updating resource_data table for resource: {uri}")
        resource_data = ResourceData(
            eli_uri=uri,
            job_id=harvester_config.job_config.job_id,
            metadata_path=path,
            metadata_timestamp=datetime.now(),
        )
        DBClient(harvester_config.db_config).update_or_create(resource_data, "eli_uri")
        logger.debug(f"resource_data table successfully updated for resource: {uri}")
        logger.debug(
            f"Triples file for resource: {uri} successfully generated and stored"
        )
        return item
