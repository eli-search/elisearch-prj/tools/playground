import logging
from datetime import datetime
from typing import TypeVar, Type, Optional

from sqlalchemy import create_engine, inspect, update, and_, select
from sqlalchemy.orm import Session
from sqlalchemy import false

from harvester.common.exceptions import DatabaseException
from harvester.common.models import QueryType
from db.db_config import DBConfig
from db.db_models import ResourceData, FilteringQuery, Base

logger = logging.getLogger(__name__)
T = TypeVar("T", bound=Base)


class DBClient:
    def __init__(self, db_config: DBConfig):
        self.db_config = db_config

    def create_session(self) -> Session:
        try:
            logger.debug("Creating a new DB session")
            engine = create_engine(
                f"{self.db_config.engine}://{self.db_config.username}:{self.db_config.password}@{self.db_config.host}:"
                f"{self.db_config.port}/{self.db_config.dbname}"
            )
            session = Session(engine)
            logger.debug("DB session successfully created")
            return session
        except Exception as err:
            raise DatabaseException("Creation of the DB session failed") from err

    def get_by_primary_key(
        self,
        resource_type: Type[T],
        primary_key: str,
        primary_key_value: str,
        session: Session,
    ) -> Optional[T]:
        try:
            logger.debug(f"Get {resource_type} by {primary_key}: {primary_key_value}")
            if not session:
                session = self.create_session()
            resource = session.get(resource_type, primary_key_value)
            logger.debug(f"{resource_type} successfully retrieved")
            return resource
        except Exception as err:
            raise DatabaseException(
                f"Failed to get {resource_type} for {primary_key}: {primary_key_value}"
            ) from err

    def update_or_create(self, resource: Type[T], primary_key_name: str) -> None:
        session = self.create_session()
        primary_key_value = resource.__dict__[primary_key_name]
        filter_criteria = {primary_key_name: primary_key_value}
        try:
            logger.debug(
                f"Looking for {type(resource)} with filter criteria: {filter_criteria}"
            )
            existing_object = self.get_by_primary_key(
                type(resource), primary_key_name, primary_key_value, session
            )
            if not existing_object:
                logger.debug(f"Inserting resource {primary_key_value}")
                session.add(resource)
                logger.debug(f"Resource {primary_key_value} successfully added")
            else:
                logger.debug(f"Updating resource {primary_key_value}")
                mapper = inspect(type(resource))
                if mapper is None:
                    raise DatabaseException(f"Failed to inspect {type(resource)}")
                mapped_columns = [c.key for c in mapper.c]
                for key, value in resource.__dict__.items():
                    if key in mapped_columns:
                        setattr(existing_object, key, value)
                logger.debug(f"Resource {primary_key_value} successfully updated")
            session.commit()
        except Exception as err:
            raise DatabaseException(
                f"Failed to update {type(resource)} for {primary_key_name}: {primary_key_value}"
            ) from err

    def mark_as_deleted(self, job_id: str, scraped_ids: set[str]) -> None:
        try:
            logger.debug(f"Updating the deleted tags for job_id: {job_id}")
            session = self.create_session()
            update_statement = (
                update(ResourceData)
                .where(
                    and_(
                        ResourceData.job_id == job_id,
                        ResourceData.eli_uri.notin_(scraped_ids),
                        ResourceData.deleted == false(),
                    )
                )
                .values(deleted=True, last_update=datetime.now())
            )
            session.execute(update_statement)
            session.commit()
            logger.debug("Deleted resources updated successfully")
        except Exception as err:
            raise DatabaseException(
                f"Failed to mark deleted resources for job id {job_id}"
            ) from err

    def get_filtering_queries(self, query_type: QueryType) -> list:
        try:
            logger.debug(f"Retrieving filtering queries for query_type: {query_type}")
            session = self.create_session()
            select_statement = select(FilteringQuery.content).where(
                and_(FilteringQuery.query_type == query_type, FilteringQuery.enabled)
            )
            result = session.execute(select_statement).all()
            logger.debug(
                f"Filtering queries for query_type: {query_type} retrieved successfully: "
                f"found {len(result)}."
            )
            return [row.content for row in result]
        except Exception as err:
            raise DatabaseException(
                f"Failed to retrieve filtering queries for query type : {query_type.value}"
            ) from err
